package com.harika.testng;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MavenTest {
	@Test
	public void f() {

		System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");

		WebElement we = driver.findElement(By.name("q"));

		we.sendKeys("w3schools");
		we.submit();
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		driver.findElement(By.linkText("HTML Tutorial")).click();

		String currenturl = driver.getCurrentUrl();
		String expectedurl = "https://www.w3schools.com/html/";
		assertEquals(currenturl, expectedurl, "target page not loaded: /html");
		/*if (expectedurl.equals(currenturl)) {
			System.out.println("Test Pass");8
		} else {
			System.out.println("Test Fail");
		}*/
	}
}
